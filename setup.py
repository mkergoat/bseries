from setuptools import setup

setup(name='BSeriesOW',
version='0.1',
description='B-Series Open Water performances',
url='#',
author='Mathieu Kergoat',
author_email='mathieu.kergoat@ensta-bretagne.org',
license='GNU GPL 3.0',
packages=['BSeriesOW'],
zip_safe=False)

