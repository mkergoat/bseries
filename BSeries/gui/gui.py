import PySimpleGUI as sg
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

sg.theme('')  # Add a touch of color
# All the stuff inside your window.
layout = [  [sg.Text('EAR'), sg.InputText()],
            [sg.Text('P/D'), sg.InputText()],
            [sg.Text('Re'), sg.InputText()],
            [sg.Text('V'), sg.InputText()],
            [sg.Text('D'), sg.InputText()],
            [sg.Button('Ok'), sg.Button('Cancel')],  
            [sg.Text('Load control file'), sg.FileBrowse()], 
            [sg.Canvas(key='-CANVAS-')] ]

# Create the Window
window = sg.Window('B-Series Performances', layout, finalize=True, element_justification='center', font='Helvetica 18')
# Event Loop to process "events" and get the "values" of the inputs

fig = plt.Figure(figsize=(5, 4), dpi=100)
t = np.arange(0, 3, .01)
fig.add_subplot(111).plot(t, 2 * np.sin(2 * np.pi * t))

def draw_figure(canvas, figure):
    figure_canvas_agg = FigureCanvasTkAgg(figure, canvas)
    figure_canvas_agg.draw()
    figure_canvas_agg.get_tk_widget().pack(side='top', fill='both', expand=1)
    return figure_canvas_agg

# add the plot to the window
fig_canvas_agg = draw_figure(window['-CANVAS-'].TKCanvas, fig)

event, values = window.read()

window.close()