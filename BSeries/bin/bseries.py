# Wageningen B-Series Open Water Curves
from collections import OrderedDict
import numpy as np
# import matplotlib2tikz as pltt
from argparse import ArgumentParser

from ..src.performances import compute_kt_kq, eta, reynolds_correction
from ..src.plot import plot_ow


def main():

    parser = ArgumentParser(description='Generates plots of the open-water performances of B-Series Propellers.')
    parser.add_argument('-i', type=bool, default=False, help="Interactive mode or not.")
    parser.add_argument('-Z', type=int,
                        help='Number of blades')
    parser.add_argument('--PoD_min', type=float,
                        help='Minimum value for the reduced pitch',
                        default=0.5)
    parser.add_argument('--PoD_max', type=float,
                        help='Maximum value for the reduced pitch',
                        default=1.4)
    parser.add_argument('--EAR', type=float,
                        default=0.5,
                        help='Extended Area Ratio')
    parser.add_argument('--Re', type=float, default=0.,
                        help='Reynolds number')
    parser.add_argument('-D', type=float, default=0.,
                        help='Propeller diameter')
    parser.add_argument('-V', type=float, default=0.,
                        help='Advance speed')
    parser.add_argument('--nu', type=float, default=1.14e-6,
                        help='Advance speed')
    args = parser.parse_args()

    if args.i:
        PoD_min = float(input('P/D min =' + '\n'))
        PoD_max = float(input('P/D max =' + '\n'))
        EAR = float(input('Ae/A0 =' + '\n'))
        Z = float(input('Z =' + '\n'))
        Re = float(input('Re =' + '\n'))
    else:
        PoD_min = args.PoD_min
        PoD_max = args.PoD_max
        EAR = args.EAR
        Z = args.Z
        if args.Re == 0.:
            Re = 0.75 * args.D / 2 * args.V / args.nu
        else:
            Re = args.Re

    print('Curves will be computed using a P/D step of 0.1, hence ' +
            '{:.0f}'.format((PoD_max - PoD_min) / 0.1) + ' curves' + '\n')

    PoDs = np.arange(PoD_min, PoD_max, 0.1)
    J = np.linspace(0, 5, 1000)
    curves = OrderedDict()
    for PoD in PoDs:
        Kt = compute_kt_kq(J, PoD, EAR, Z)[0]
        Kq = compute_kt_kq(J, PoD, EAR, Z)[1]
        eta0 = eta(J, Kt, Kq)
        if Re > 2e6:
            Kt = Kt + reynolds_correction(J, PoD, Re, EAR, Z)[0]
            Kq = Kq + reynolds_correction(J, PoD, Re, EAR, Z)[1]
        else:
            pass
        for k in range(len(Kt)):
            if Kt[k] < 0:
                q = k
                break
        curves['{}'.format(PoD)] = OrderedDict({'KT': Kt[:q], 'KQ': Kq[:q], 'eta0': eta0[:q]})    
    fig = plot_ow(J, EAR, Z, Re, curves)

if __name__ == "__main__":
    main()
