import matplotlib.pyplot as plt

COLORS = ['r', 'g', 'b', 'c', 'm', 'y', 'k', 'violet', 'purple', 'fuchsia']

LINESTYLES = ['solid', 'dotted', 'dashed']

def plot_ow(J, EAR, Z, Re, curves):
    fig = plt.Figure(figsize=(5, 4), dpi=100)   
    ax = fig.add_subplot(111)
    for i, c in enumerate(curves):
        ax.plot(J, curves[c]['KT'], label='P/D={0}'.format(
            round(c, 2)), linestyle=LINESTYLES[0], color=COLORS[i])
        ax.plot(J, 10 * curves[c]['KQ'], linestyle=LINESTYLES[1], color=COLORS[i])
        ax.plot(J, curves[c]['eta0'], linestyle=LINESTYLES[2], color=COLORS[i])
        #plt.text(J[list(eta0).index(max(eta0))] - 0.1, max(eta0) + 0.05, 'P/D=' + str(PoDs[i]))
    ax.xlabel('Advance parameter $J$')
    ax.ylabel('$K_T$ (solid), $5K_Q$ (dotted), $\eta_0$ (dashed)')
    ax.legend()
    ax.title('Open-water curves for B-series $A_E/A_0=$' +
            str(EAR) + ', $Z=$' + str(int(Z)) + ' at Re = ' + str(Re))
    plt.savefig('OW_{}_{:.0f}.svg'.format(Z, 100 * EAR))
    return fig