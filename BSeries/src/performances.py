from math import log, pi


def compute_kt_kq(J, PoD, EAR, Z):
    f = open('coeffs.dat', 'r')
    f.readline()
    a = f.readlines()
    f.close()
    kt = 0.
    kq = 0.
    for line in a:
        l = line.split(' ')
        kq += float(l[0]) * J ** float(l[1]) * PoD ** float(l[2]) * \
            EAR ** float(l[3]) * Z ** float(l[4])
        kt += float(l[5])* J ** float(l[6]) * PoD ** float(l[7]) * \
            EAR ** float(l[8]) * Z ** float(l[9])
    return kt, kq


def reynolds_correction(J, PoD, Re, EAR, Z):
    delta_kt = 0.000353485
    - 0.0033375800 * EAR * J ** 2
    - 0.0047812500 * EAR * PoD * J
    + 0.0002577920 * (log(Re) - 0.301) ** 2 * EAR * J ** 2
    + 0.0000643192 * (log(Re) - 0.301) * PoD ** 6 * J ** 2
    - 0.0000110636 * (log(Re) - 0.301) ** 2 * PoD ** 6 * J ** 2
    - 0.0000276305 * (log(Re) - 0.301) ** 2 * Z * EAR * J ** 2
    + 0.0000954500 * (log(Re) - 0.301) * Z * EAR * PoD * J
    + 0.0000032049 * (log(Re) - 0.301) * Z ** 2 * EAR * PoD ** 3 * J
    delta_kq = -0.000561412
    + 0.00696898000 * PoD
    - 0.00006666540 * Z * PoD ** 6
    + 0.01608180000 * EAR ** 2
    - 0.00093809100 * (log(Re) - 0.301) * PoD
    - 0.00059593000 * (log(Re) - 0.301) * PoD ** 2
    + 0.00007820990 * (log(Re) - 0.301) ** 2 *PoD ** 2
    + 0.00000521990 * (log(Re) - 0.301) * Z * EAR * J ** 2
    - 0.00000088528 * (log(Re) - 0.301) ** 2 * Z * EAR * PoD * J
    + 0.00002301710 * (log(Re) - 0.301) * Z * PoD ** 6
    - 0.00000184341 * (log(Re) - 0.301) ** 2 * Z * PoD ** 6
    - 0.00400252000 * (log(Re) - 0.301) * EAR ** 2
    + 0.00022091500 * (log(Re) - 0.301) ** 2 * EAR ** 2
    return delta_kt, delta_kq


def eta(J, Kt, Kq):
    return J * Kt / (2 * pi * Kq)